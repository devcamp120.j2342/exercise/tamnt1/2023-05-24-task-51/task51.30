import java.util.regex.Pattern;

public class App {
    public static void main(String[] args) throws Exception {
        subTask1("bananas");
        subTask2("bananas");
        subTask3("abc", "def");
        subTask4();
        subTask5();
        subTask6();
        subTask7();
        subTask8();
        subTask9();
        subTask10();
        subTask11();
        subTask12();
        subTask13();
        subTask14();
        subTask15();
        subTask16();
        subTask17();

    }

    public static void subTask1(String str) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            char ch = str.charAt(i);
            if (sb.indexOf(String.valueOf(ch)) == -1) {
                sb.append(ch);
            }
        }
        System.out.println(sb.toString());
    }

    public static void subTask2(String str) {
        str = str.trim();
        if (str.isEmpty()) {
            return;
        }
        String[] words = str.split("\\s+");
        System.out.println(words.length);

    }

    public static void subTask3(String str1, String str2) {
        String result1 = str1 + str2;
        System.out.println(result1);
    }

    public static void subTask4() {
        String s1 = "Devcamp java";
        String s2 = "java";

        boolean containsSubstring = s1.contains(s2);
        System.out.println("Chuỗi s1 có chứa chuỗi s2: " + containsSubstring);
    }

    public static void subTask5() {
        String str = "Devcamp java";
        int k = 3;

        if (k >= 0 && k < str.length()) {
            char character = str.charAt(k);
            System.out.println("Ký tự thứ " + k + " trong chuỗi: " + character);
        } else {
            System.out.println("Vị trí k không hợp lệ");
        }
    }

    public static void subTask6() {
        String str = "Devcamp java";
        char target = 'a';
        int count = 0;

        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == target) {
                count++;
            }
        }

        System.out.println("Số lần xuất hiện của ký tự '" + target + "' trong chuỗi: " + count);
    }

    public static void subTask7() {
        String str = "Devcamp java";
        char target = 'a';

        int firstOccurrence = str.indexOf(target);

        if (firstOccurrence != -1) {
            System.out.println(
                    "Vị trí xuất hiện lần đầu tiên của ký tự '" + target + "' trong chuỗi: " + firstOccurrence);
        } else {
            System.out.println("Ký tự '" + target + "' không xuất hiện trong chuỗi.");
        }

    }

    public static void subTask8() {
        String str = "Devcamp";
        String strUpper = str.toUpperCase();

        System.out.println("Chuỗi chuyển sang in hoa: " + strUpper);
    }

    public static void subTask9() {
        String str = "DevCamp";
        StringBuilder result = new StringBuilder();

        for (char c : str.toCharArray()) {
            if (Character.isUpperCase(c)) {
                result.append(Character.toLowerCase(c));
            } else if (Character.isLowerCase(c)) {
                result.append(Character.toUpperCase(c));
            } else {
                result.append(c);
            }
        }

        String convertedStr = result.toString();
        System.out.println("Chuỗi sau khi chuyển đổi: " + convertedStr);
    }

    public static void subTask10() {
        String str = "DevCamp";
        int count = 0;

        for (char c : str.toCharArray()) {
            if (Character.isUpperCase(c)) {
                count++;
            }
        }
        System.out.println("Số ký tự in hoa trong chuỗi: " + count);
    }

    public static void subTask11() {
        String str = "DevCamp123";
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (!Character.isDigit(c)) {
                sb.append(c);
            }

        }
        System.out.println(sb.toString());
    }

    public static void subTask12() {
        String str = "abcdefghijklmnopqrstuvwxy";
        int n = 5;
        int length = str.length();

        if (length % n != 0) {
            System.out.println("KO");
            return;
        }

        for (int i = 0; i < length; i += n) {
            String part = str.substring(i, i + n);
            System.out.println(part);
        }

    }

    public static void subTask13() {
        String str = "aabaarbarccrabmq";
        StringBuilder sb = new StringBuilder(str);
        boolean foundDuplicates = true;

        while (foundDuplicates) {
            foundDuplicates = false;
            for (int i = 0; i < sb.length() - 1; i++) {
                if (sb.charAt(i) == sb.charAt(i + 1)) {
                    sb.delete(i, i + 2);
                    foundDuplicates = true;
                }
            }
        }

        System.out.println(sb.toString());
    }

    public static void subTask14() {
        String str1 = "Welcome";
        String str2 = "home";
        int len1 = str1.length();
        int len2 = str2.length();

        while (len1 < len2) {
            str2 = str2.substring(1);
            len2--;
        }

        while (len2 < len1) {
            str1 = str1.substring(1);
            len1--;
        }

        String result = str1 + str2;
        System.out.println(result);
    }

    public static void subTask15() {
        String str = "DevCamp";
        StringBuilder result = new StringBuilder();

        for (char c : str.toCharArray()) {
            if (Character.isUpperCase(c)) {
                result.append(Character.toLowerCase(c));
            } else if (Character.isLowerCase(c)) {
                result.append(Character.toUpperCase(c));
            } else {
                result.append(c);
            }
        }

        String convertedStr = result.toString();
        System.out.println("Chuỗi sau khi chuyển đổi: " + convertedStr);
    }

    public static void subTask16() {
        String str = "DevCamp123";

        for (char c : str.toCharArray()) {
            if (Character.isDigit(c)) {
                System.out.println("true");
            }
        }

        System.out.println("false");
    }

    public static void subTask17() {
        String str = "DevCamp123";
        String pattern = "^[A-Z][a-zA-Z0-9]{0,18}[0-9]$";

        Pattern regex = Pattern.compile(pattern);

        boolean isMatch = regex.matcher(str).matches();
        System.out.println(isMatch);
    }
}
